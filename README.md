# BootDataDemo

The BootDataDemo is a small Spring Boot application that primarily demonstrates Spring Data REST repositories (with JPA), with an H2 database residing on the local filesystem.  The H2 database is bundled within the project, and contains prepopulated sample data.  The REST Repositories HAL Browser is also included, which provides a web interface to browse the REST repositories.

## Instructions

The most common tasks are listed below.  These utilize the Gradle wrapper included in the repository.  Note, "gradlew.bat" is used in the examples, but you can use "gradlew" on Linux and OS X systems.

### Run

    gradlew.bat run

### Create Distributable Zip

    gradlew.bat distZip

### Generate Eclipse Project Files

    gradlew.bat cleanEclipse eclipse
    
## Accessing the Running Application 

The HAL Browser - http://localhost:8080