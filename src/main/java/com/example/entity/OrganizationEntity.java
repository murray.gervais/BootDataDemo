package com.example.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ORGANIZATION")
public class OrganizationEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false, insertable = false, updatable = false)
	private Long id;
	
	@Column(name = "NAME", nullable = false, length = 64, unique = true)
	private String name;
	
	@OneToMany(mappedBy = "organization")
	private Set<EmployeeEntity> employees;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Set<EmployeeEntity> getEmployees()
	{
		return employees;
	}

	public void setEmployees(Set<EmployeeEntity> employees)
	{
		this.employees = employees;
	}

}