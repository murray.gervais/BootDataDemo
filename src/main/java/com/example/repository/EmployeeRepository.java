package com.example.repository;

import java.util.Set;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.entity.EmployeeEntity;

public interface EmployeeRepository extends PagingAndSortingRepository<EmployeeEntity, Long>
{
	public Set<EmployeeEntity> findByLastNameLike(String lastName);
}
