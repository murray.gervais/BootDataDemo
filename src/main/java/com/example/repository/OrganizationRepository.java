package com.example.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.entity.OrganizationEntity;

@Repository
public interface OrganizationRepository extends PagingAndSortingRepository<OrganizationEntity, Long>
{
	public OrganizationEntity findByName(String name);
}
